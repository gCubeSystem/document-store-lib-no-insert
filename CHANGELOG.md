This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Document Store Backend Connector Library No Insert


## [v2.0.0] 

- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure


## [v1.2.0] [r4.12.1] - 2018-10-10

- Creating uber-jar instead of jar-with-dependencies [#10155]


## [v1.1.0] [r4.7.0] - 2017-10-09

- Implemented isConnectionActive() in PersistenceBackend provided implementation


## [v1.0.0] [r4.5.0] - 2017-06-07

- First Release
